# Clean
rm -rf ./.fork ./*/ ./index.ts{x,} ./README.md

# Fetch Upstream
git clone --depth=1 https://github.com/philhk/Vencord .fork
rm -rf ./.fork/.git

# Grab plugin files
echo "Copying betterScreenshare..."
cp -r .fork/src/plugins/betterScreenshare.desktop/* ./
find ./{*/,index.tsx} -type f | xargs sed -i 's/"\.\.\/\(\(\.\.\/\)*\)philsPluginLibrary/".\/\1lib/'
find ./{*/,index.tsx} -type f | xargs sed -i 's/"\.\.\/\(\(\.\.\/\)*\)betterMicrophone\.desktop/"\1lib\/betterMicrophone/'

echo "Copying philsPluginLibrary..."
mkdir ./lib
cp -r .fork/src/plugins/philsPluginLibrary/* ./lib/
find ./lib/ -type f | xargs sed -i 's/"\.\.\/\(\(\.\.\/\)*\)philsPluginLibrary/"\1/'
find ./lib/ -type f | xargs sed -i 's/"\.\.\/\(\(\.\.\/\)*\)betterMicrophone\.desktop/".\/\1betterMicrophone/'
find ./lib/ -type f | xargs sed -i 's/"\(\(\.\.\/\)*\)betterScreenshare\.desktop\/\?/".\/\1/'

echo "Copying betterMicrophone..."
mkdir ./lib/betterMicrophone
cp -r .fork/src/plugins/betterMicrophone.desktop/* ./lib/betterMicrophone
find ./lib/betterMicrophone -type f | xargs sed -i 's/"\.\.\/\(\(\.\.\/\)*\)philsPluginLibrary\/\?/"..\/\1/'

echo "Applying final patches..."
find ./{*/,index.tsx} -type f | xargs sed -i 's/Devs.philhk/({ name: "philhk", id: 305288513941667851n })/'
find ./{*/,index.tsx} -type f | xargs sed -i 's/import TypedEmitter from "typed-emitter";/type TypedEmitter<T> = InstanceType<typeof import("events").EventEmitter> \& { map: T };/'
sed -i 's/ } from "\.\/lib"/, default as LibPlugin&/' index.tsx
sed -i '/this\.patches = \[/a ...LibPlugin.patches,' index.tsx
sed -i 's/this.dependencies = .*;/Object.assign(this, Object.fromEntries(Object.entries(LibPlugin).filter(([key]) => !Object.hasOwn(this, key))));/' index.tsx
sed -i '/readonly dependencies/d' index.tsx

sed -i "1a > ### **THIS IS A MIRROR REPOSITORY**" README.md
sed -i "2a > The original code is located at https://github.com/philhk/Vencord/tree/better-plugins" README.md

if [[ -n $DO_LINTING ]]; then
    echo "Linting..."
    pnpm eslint src/userplugins/betterScreenshare.desktop --ext .js,.jsx,.ts,.tsx --fix
fi
